const Driver = require('../models/driver');

module.exports = {
  //greeting: function (req, res) {}
  greeting(req, res) {
    res.send({ hi: 'there' });
  },

  index(req, res, next) {
    const { lng, lat } = req.query;//get the lat and lng from the URL of the GET request ?lng=xyz&lat=abc
    Driver.aggregate([
      {
        $geoNear: {
          near: { type: 'Point', coordinates: [parseFloat(lng), parseFloat(lat)] },
          distanceField: "dist.calculated",
          includeLocs: 'dist.location',
          maxDistance: 200000,
          spherical: true,
        }
      }
      ]).then(drivers => res.send(drivers))
      .catch(next);
    // Driver.geoSearch()
  },

  create(req, res, next) {
    const driverProps = req.body;//get the email from the POST request body

    Driver.create(driverProps)//inserta registro en tabla
      .then(driver => res.send(driver))
      .catch(next);//call the middleware on app.js
  },

  edit(req, res, next) {
    const driverId = req.params.id;//get the id from the URL of the GET request /:id
    const driverProps = req.body;

    Driver.findByIdAndUpdate({ _id: driverId }, driverProps)
      .then(() => Driver.findById({ _id: driverId }))
      .then(driver => res.send(driver))
      .catch(next);
  },

  delete(req, res, next) {
    const driverId = req.params.id;

    Driver.findByIdAndRemove({ _id: driverId })
      .then(driver => res.status(204).send(driver))
      .catch(next);
  }
};