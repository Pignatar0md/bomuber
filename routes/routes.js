const DriverController = require('../controllers/drivers_controller');
const Driver = require('../models/driver');

module.exports = (app) => {
  app.get('/api', DriverController.greeting);
  // app.get('/api', (req, res) => {
  //   res.send({ hi: 'there' });
  // });
  app.post('/api/drivers', DriverController.create);

  app.put('/api/drivers/:id', DriverController.edit);

  app.delete('/api/drivers/:id', DriverController.delete);

  app.get('/api/drivers/', DriverController.index);
};