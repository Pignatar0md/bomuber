const mongoose = require('mongoose');

before(done => {
  mongoose.connect('mongodb://localhost/muber_test', { useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify: false });
  mongoose.set('useCreateIndex', true);
  let db = mongoose.connection;
  db.once('open', () => done())
    .on('error', err => {
      console.warn('warninggg!', err);
    });
});

beforeEach(done => {
  const { drivers } = mongoose.connection.collections;
  drivers.drop() 
    .then(() => drivers.ensureIndex({ 'geometry.coordinates': '2dsphere' }))
    .then(() => done())
    .catch(() => done());
});