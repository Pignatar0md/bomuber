const assert = require('assert');
const request = require('supertest');
const mongoose = require('mongoose');
const app = require('../../app');

const Driver = mongoose.model('driver');

describe('My drivers controller', () => {

  it('Post to /api/drivers and creates a new driver', done => {
    Driver.countDocuments().then(count => {//how many drivers are registered BEFORE adding another one
      request(app)
        .post('/api/drivers')
        .send({ email: 'test@test.com' })
        .end(() => {
          
          Driver.countDocuments().then(newCount => {//how many drivers are registered AFTER adding another one
            assert(count + 1 === newCount);
            done();
          });
        });
    });
  });

  it('Put to /api/drivers/id and edits an existing driver', done => {
    const driver = new Driver({ email: 'test9@test.com', driving: false });
    driver.save().then(() => {
      request(app)
        .put(`/api/drivers/${driver._id}`)//when we create a driver, it returns its id
        .send({ driving: true })
        .end(() => {
          
          Driver.findOne({ email: 'test9@test.com' })
            .then(driver => {
              assert(driver.driving === true);
              done();
            });
        });
    });
  });

  it('Delete to /api/drivers/id can delete a driver', done => {
    const driver = new Driver({ email: 'test9@test.com', driving: false });
    driver.save().then(() => {
      request(app)
        .delete(`/api/drivers/${driver._id}`)
        .end(() => {

          Driver.findOne({ email: 'test9@test.com' })
            .then(driver => {
              assert(driver === null);
              done();
            });
        });
    });
  });

  it('get to /api/drivers finds drivers in a location', done => {
    const driver1 = new Driver({
      email: 'seattle@test.com',
      geometry: { type: 'Point', coordinates: [-122.4759902, 47.6147628] } 
    });
    const driver2 = new Driver({
      email: 'miami@test.com',
      geometry: { type: 'Point', coordinates: [-80.253, 25.791] }
    });

    Promise.all([ driver1.save(), driver2.save() ])
      .then(() => {
        request(app)
          .get('/api/drivers?lng=-80&lat=25')
          .end((err, response) => {
            // console.log(response.body[0].email);
            assert(response.body.length === 1);
            assert(response.body[0].email === 'miami@test.com');
            done();
          });
      });
  });
});